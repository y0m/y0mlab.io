---
title: 'About me'
date: '2018-08-15'
---

My name is Guillaume.

One could think my progression has been a bit erratic: from C language to ASP (because I had VB notions), a bit of ColdFusion, ASP.NET because C# was attractive and leading technology in 2001. But clearly, my first love was C so that's why I left the Microsoft world for FreeBSD, with the really appreciated help of my long run best friend.

I almost always try to keep a concept in my mind during my progression, KISS!: Keep It Simple, Stupid!
You can check here a quick list about what I've been doing.


Recently learnt:
* [Go language](https://golang.org/), it is quite opinionated but really nice to use.
* [Rust](https://www.rust-lang.org/), I like the no runtime and memory safety.
* [Zig](https://ziglang.org/), no runtime as well, and like it too, closer to C than Rust.


Since highschool graduation:
* Since Oct. 2012 : **Societe.com**, Systems administrator, DevOps, R&D 
* Nov. 2010 to Oct. 2012: **Societe.com**, R&D engineer
* April 2009 to Oct. 2010: **Winwise** (Provision of services, Microsoft.Net certification affiliate gold)
* March 2005 to March 2009: **Baobaz** (Web Agency), .NET engineer (C#, ASP.Net)
* Nov. 2004 to Feb. 2005: **Planaxis France** (Provision of services), Coder engineer (ASP and COM/COM+ Objects)
* July 2003 to Oct. 2004: French Internet bubble crisis 2001/2003, I learned a few things:
  * [Perl](http://www.perl.org/), [Mono](http://www.mono-project.com/), C, [GTK+](http://www.gtk.org/), created my own distro using [Linux From Scratch](http://www.linuxfromscratch.org/lfs/) BSD starting scripts
* April 1999 to June 2003: **Fi System** (Provision of services) Coder engineer (ASP, ColdFusion, ASP.Net)
* Nov. 1998 to March 1999: **InFine Conseil** (Provision of services), Coder and analyst (C, AIX, Sun)
* July 1998 to Oct. 1998: **Illico** (Provision of services), Coder and analyst (Cobol, let's pretend 'not')
* April 1998 to June 1998: **Banque Sofinco**, Coder and analyst, End of college stage (VB6, Access)
* Sept. 1996 to June 1998: **I.U.T d'Orsay**, Paris XI University, D.U.T. Informatique (Systems & networks)
* June 1996: **Lycée J.B. Corot**, Savigny-sur-Orge, Scientific Baccalauréat (Math speciality)


_Informations gathered in this website, and especially opinions with the "Personal" label are the sole responsability of me._
