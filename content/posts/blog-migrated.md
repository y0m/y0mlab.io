---
title: 'Blog migrated!'
date: 2019-01-08T23:24:00.000+02:00
draft: false
tags: [Personal]
---

It's been a while since I first created my blog on Blogger, but I really needed a more simple solution at the time. I guess and think I've found it in [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and [Hugo](https://gohugo.io/) SSG!

So... **Welcome** to my new blog!

I've removed almost all ancient/irrelevant/obsolete posts which I can't really upgrade to current technologies (even if I could). Please bear with it :)
