---
title: 'DigitalOcean Volume and FreeBSD Droplet'
date: 2016-10-17T15:09:00.002+02:00
draft: false
tags : [Configuration, FreeBSD]
---

Earlier this year, I decided not to throw as much money in some dedicated online server (dedicated racked server) from Online.net. Not that the service would have been horrible, but with my current budget I could not offer myself such a high level service anymore.  

  

So and since I had already tested DO (DigitalOcean) VPS (Droplets), I had migrated my mail server there. It was a simple 40GB / 2GB / 2CPU VPS with ZFS installed until recently, so with just one disk.

  

I've just taken some time to read about the new "volume" functionality they offer, and read it's possible to add some DO Volume to ZFS with ease. So I created a new 40GB volume, and attached it to my droplet. FreeBSD discovered the disk instantly (transfer rate seems insane to me):

  

{{< highlight plain >}}
(da0:vtscsi0:0:0:1): UNMAPPED  
da0 at vtscsi0 bus 0 scbus2 target 0 lun 1  
da0: Fixed Direct Access SPC-3 SCSI device  
da0: 344054.232MB/s transfers  
da0: Command Queueing enabled  
da0: 40960MB (83886080 512 byte sectors)
{{< /highlight >}}

  
Then, I partitioned the disk so that it's the same GPT scheme:  
  

{{< highlight bash >}}
gpart create -s GPT da0  
gpart add -t freebsd-boot -a 4k -s 512k -l gptboot1 da0 # there was a gptboot0 already  
gpart bootcode -b /boot/pmbr -p /boot/gptzfsboot -i 1 da0  
gpart add -t freebsd-swap -a 1m -s 2gb da0  
gpart add -t freebsd-zfs -a 1m da0   
{{< /highlight >}}

  
and then, attached the freshly partitioned zfs volume to the existing pool:  
  

{{< highlight bash >}}
zpool attach zroot vtbd0p3 da0p3
{{< /highlight >}}

  
After a few minutes, ZFS had resilvered the data:  
  

{{< highlight plain >}}
zpool status  
  pool: zroot  
 state: ONLINE  
  scan: resilvered 7.57G in 0h14m with 0 errors on Mon Oct 17 14:32:29 2016  
config:  
  
        NAME         STATE     READ WRITE CKSUM  
        zroot        ONLINE       0     0     0  
          mirror-0   ONLINE       0     0     0  
            vtbd0p3  ONLINE       0     0     0  
            da0p3    ONLINE       0     0     0  
  
errors: No known data errors  
{{< /highlight >}}

  
Just that fast and easy, and my data are now mirrored. :D
