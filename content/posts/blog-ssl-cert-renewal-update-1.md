---
title: "Blog SSL certificate renewal - Update"
date: 2024-03-21T10:22:00.000+01:00
draft: false
tags : ['GitLab', 'CI-CD', 'LetsEncrypt']
---

It's been a while since previously posting, since it's been busy years and I 
couldn't find the time to update.
I was using a custom method to update my Let's Encrypt certificate for this site, 
but since then, it has been nicely integrated into Gitlab and so... it's a burden 
less to monitor and it's getting updated automatically.

It's sometimes nice to have a full control over some data, but in that case: a 
personal web log, it doesn't really matter much.

Reference: https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html
