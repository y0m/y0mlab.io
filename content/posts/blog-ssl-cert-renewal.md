---
title: "Blog SSL certificate renewal"
date: 2020-10-08T22:20:00.002+02:00
draft: false
tags : ['GitLab', 'CI-CD', 'Docker', 'LetsEncrypt']
---

So, if you've been following my past blog posts, you may have read this one:
[Blog migrated!](https://yom.iaelu.net/2019/01/blog-migrated.html).
But just in case:
* this blog is running with the help of gitlab.com pages feature

I'm using a free account, so this means I'm not paying a penny for this blog to be served. It's no big deal, there are many other services like this on Internet. I'm just using the GitLab one because it's serving my purpose.

I always wanted my blog SSL certificate to get renewed automatically, and for "free". And IT IS DONE! I'm using a mix of GitLab public and private project. The private project acts as sensible data storage, I'm the only one who can see and modify it. The public project is to use GitLab CI/CD free quota for pipelines. Yeah... only private projects pipeline get counted for the quota.

And also, I'm using Let's Encrypt certificates services which are quite good enough anyway, even if they are renewed every 3 months or so.

## The private repository
I can't show you exactly what my private repo is, but at least I can tell you what's inside.
My private repo is holding the files created by [`certbot`](https://certbot.eff.org/) when you create your first Let's Encrypt certificate for your website.
I've got 3 folders:
* `config`, in which I'm storing the `/etc/letsencrypt/` subfolders
* `log`, in which just the most recent log is stored, that should be `/var/log/letsencrypt/letsencrypt.log`
* `lib`, which acts as a dummy `/var/lib/letsencrypt` folder, I'm not even sure temporary files are created during the renewal

## The public repository
You can check it, I think I made it safe enough so that my data are supposed to be safe:
[iaelu/yom-iaelu-net-renewal](https://gitlab.com/iaelu/yom-iaelu-net-renewal)

This repo is holding the [`.gitlab-ci.yml`](https://gitlab.com/iaelu/yom-iaelu-net-renewal/-/blob/master/.gitlab-ci.yml) that's called on a schedule configured in the CI/CD part of the project. It's launching a shell script that will clone the private repo and use it inside a docker image from `certbot`.

I've configured a few CI/CD variables that are protected and masked to help me keep most of the secret data hidden, but if you can read shell script, then it should be really easy to understand my script, which is quite simple: [`script/renew.sh`](https://gitlab.com/iaelu/yom-iaelu-net-renewal/-/blob/master/script/renew.sh).

## P.S.
I'm using Google Cloud DNS, it's not free, but my domain is not queried that much so this means I've got a monthly bill of $0.31/mo for this. Not free... but I don't have to search for tricks since the `certbot/dns-google` docker image does everything it needs to do with the DNS with a secret OAUTH file.
Of course, all these are still serving my purpose: learning new things, applying my knowledge in a fun way.
