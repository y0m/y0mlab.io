---
title: 'C Library for CGI'
date: 2012-05-10T10:39:00.000+02:00
draft: false
tags : [Coding, FreeBSD]
---

I've been looking for a C library to create CGI, and found a page that's listing some :  
[Comparison of CGI libraries in C](http://www.lemoda.net/c/cgi-libraries/index.html)  
  
Some are simple, maybe too much..., and some look complete, but too much complex and/or the code doesn't suit my requirements, or the licence used is clearly not one I'd like to use also.  
  
So I've decided to develop my own, a simple CGI C library, which try to be :  
  

1.  **Simple** : There's the minimum method to use it. Most methods name are quiet easily understandable and they do no more no less what they should do.
2.  **Headers try to be delegated**, which means a header should be responsible for its life cycle and how it gets into the Headers List. There is also the minimum for the 2 already developed headers inside this library, and one who would want to create a new header can check these two as an example to develop a new one.

  

The library is called `libSCGI` : `S` for simple, `CGI` for CGI (really... :p). You can find the source on GitLab : [https://gitlab.com/iaelu/libscgi](https://gitlab.com/iaelu/libscgi)

  

Right now, there is no version number for this library, it's clearly oriented to be build and used on **FreeBSD** since I'm using **FreeBSD Makefiles**. I'm also using **Tail Queue** `#include <sys/queue.h>`, but I guess this is also on Linux ... I didn't try it though, I do not intend to maintain a version for Linux since that's clearly not my primary goal. My primary goal is to deliver a really simple C library.

  

I'm using **3-clause BSD Licence**, since I really prefer this licence, and also it's the one used in FreeBSD.

  
I know most people are now using _New Languages_ or _Frameworks_, but I clearly prefer to use C for now. Hope some will like the library :)
